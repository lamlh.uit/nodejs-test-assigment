const express = require('express');

const router = express.Router();
const airtableController = require('../controller/airtable.controller');

// Hierarchy view
router.get("/hierarchy", airtableController.hierarchy)
// Drawings view and drawing output
router.get("/drawings", airtableController.drawings)
// Service planner
router.get("/services", airtableController.services)

module.exports = router;