const Airtable = require('airtable');

Airtable.configure({
    endpointUrl: process.env.AIRTABLE_ENDPOINT_URL,
    apiKey: process.env.AIRTABLE_API_KEY,
    apiVersion: 0
});

const base = Airtable.base(process.env.AIRTABLE_BASE_ID)

const getListRecords = async (tableName) => {
    const records = await base(tableName)
        .select().all();
        
    return records.map((x) => convertResult(x))
};

const getOneRecord = async (tableName, id) => {
    const record = await base(tableName).find(id);

    return convertResult(record);
};

const createRecord = async (tableName, fields) => {
    const record = await base(tableName).create(fields);

    return convertResult(record);
};

const updateRecord = async (tableName, id, fields) => {
    const record = await base(tableName).update(id, fields)

    return convertResult(record);
};

const deleteRecord = async (tableName, id) => {
    const result = await base(tableName).destroy(id);

    return convertResult(result);
};

const convertResult = (record) => {
    return {
        id: record.id,
        fields: record.fields,
      };
}
module.exports = {
  getListRecords,
  getOneRecord,
  createRecord,
  updateRecord,
  deleteRecord,
};
