# NodeJS Test Assignment

## Project description
This project will get data from Airtable.com and return to the user.

Framework/Library to be used:
- Express
- Nodemon
- Airtable.js
- Dotenv

## How to run the project
```npm run start```

## API structure
### 1. Hierarchy view
URL: http://localhost:3000/v1/airtable/hierarchy

Method: GET

### 2. Drawings view and drawing output
URL: http://localhost:3000/v1/airtable/drawings

Method: GET

### 3. Service planner
URL: http://localhost:3000/v1/airtable/services

Method: GET