const AirtableLib = require('../libs/airtable');
const moment = require('moment');

const hierarchy = async (req, res) => {
    try {
        // Get all models
        const foundModelList = await AirtableLib.getListRecords("models")

        // Get all mapping
        const foundMappingList = await AirtableLib.getListRecords("model_model");

        // Loop through models and map with children
        for (let i = 0; i < foundModelList.length; i+=1) {
            foundModelList[i].children = [];
            if (foundModelList[i].fields.children && foundModelList[i].fields.children.length > 0) {
                // Find children number list
                let foundChildrenNumberList = [];
                for (const item of foundMappingList) {
                    if (foundModelList[i].fields.children.includes(item.id)) {
                        foundChildrenNumberList = foundChildrenNumberList.concat(item.fields.number);
                    }
                }

                // Find children and push to array
                for (const item of foundModelList) {
                    if (foundChildrenNumberList.includes(item.id)) {
                        foundModelList[i].children.push({
                            id: item.id,
                            number: item.fields.number
                        })
                    }
                }
            }            
        }

        // Get number out of fields
        for (let i =0; i < foundModelList.length; i+=1){
            foundModelList[i].number = foundModelList[i].fields.number;
            delete foundModelList[i].fields;
        }
        return res.status(200).send(foundModelList);
    } catch (e) {
        console.log(e);
        return res.status(500).json(e);
    }
}

const drawings = async (req, res) => {
    try {
        // Get all drawings
        const foundDrawingslList = await AirtableLib.getListRecords("drawings")

        // Get all mapping
        const foundMappingList = await AirtableLib.getListRecords("model_model");

        // Loop through drawings and map with mapping
        for (let i = 0; i < foundDrawingslList.length; i+=1) {
            foundDrawingslList[i].model_model = [];
            if (foundDrawingslList[i].fields.model_model && foundDrawingslList[i].fields.model_model.length > 0) {
                foundDrawingslList[i].model_model = []
                for (const item of foundMappingList) {
                  if (
                    foundDrawingslList[i].fields.model_model.includes(item.id)
                  ) {
                    foundDrawingslList[i].model_model.push({
                      id: item.id,
                      number: item.fields.number,
                    });
                  }
                }
            }            
        }

        // Get name out of fields
        for (let i =0; i < foundDrawingslList.length; i+=1){
            foundDrawingslList[i].name = foundDrawingslList[i].fields.name;
            delete foundDrawingslList[i].fields;
        }
        return res.status(200).send(foundDrawingslList);
    } catch (e) {
        console.log(e);
        return res.status(500).json(e);
    }
}

const services = async (req, res) => {
    try {
        // Get all services
        const foundServicelList = await AirtableLib.getListRecords("services");

        // Loop through services and process dateToDoneList
        // For the purpose of demo, only add 7 elements to dateToDone list
        for (let i = 0; i < foundServicelList.length; i += 1) {
          foundServicelList[i].dateBeDoneList = [];
          if (foundServicelList[i].fields.calendar_interval) {
            let currentDate = moment();
            for (let j = 1; j <= 7; j += 1) {
              foundServicelList[i].dateBeDoneList.push(
                currentDate
                  .add(
                    +foundServicelList[i].fields.calendar_interval,
                    foundServicelList[i].fields.calendar_interval_unit
                  )
                  .format("YYYY-MM-DD HH:mm:ss")
              );
            }
          } else {
            let currentDate = moment();
            for (let j = 1; j <= 7; j += 1) {
                  foundServicelList[i].dateBeDoneList.push(
                    currentDate
                      .add(
                        +foundServicelList[i].fields.running_hours_interval,
                        "hours"
                      )
                      .format("YYYY-MM-DD HH:mm:ss")
                  );
            }
          }
        }

        // Get name out of fields
        for (let i =0; i < foundServicelList.length; i+=1){
            foundServicelList[i].name = foundServicelList[i].fields.name;
            delete foundServicelList[i].fields;
        }

        return res.status(200).send(foundServicelList);
    } catch (e) {
        console.log(e);
        return res.status(500).json(e);
    }
}
module.exports = {
    hierarchy,
    drawings,
    services
}