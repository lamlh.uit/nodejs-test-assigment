const express = require('express');
const app = express()
const port = 3000

// Load env
require('dotenv').config()

app.get('/', (req, res) => {
  res.send('NodeJS Test Assigment app!')
})

require('./routes/index.route')(app)
console.log(process.env)

app.listen(port, () => {
  console.log(`NodeJS Test Assigment app listening on port ${port}`)
})